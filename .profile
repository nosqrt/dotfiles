source "${HOME}/.bash_profile"
export GPG_TTY=$(tty)

# Flatpak exports: system
[[ -d "/var/lib/flatpak/exports/bin" ]] && PATH+=":/var/lib/flatpak/exports/bin"
[[ -d "/var/lib/flatpak/exports/share" ]] && XDG_DATA_DIRS+=":/var/lib/flatpak/exports/share"

# Flatpak exports: user
[[ -d "${HOME}/.local/share/flatpak/exports/bin" ]] && PATH+=":${HOME}/.local/share/flatpak/exports/bin"
[[ -d "${HOME}/.local/share/flatpak/exports/share" ]] && XDG_DATA_DIRS+=":${HOME}/.local/share/flatpaks/exports/share"

# Add user scripts
[[ -d "${HOME}/.local/bin" ]] && PATH+=":${HOME}/.local/bin"

# Add Perl CPAN modules
[[ -d "${HOME}/perl5/bin" ]] && PATH+=":${HOME}/perl5/bin"

export PATH
export XDG_DATA_DIRS
