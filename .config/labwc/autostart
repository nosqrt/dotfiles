# Example autostart file

# Needed for xdg-desktop-portal-wlr 
/usr/bin/dbus-update-activation-environment --systemd DISPLAY WAYLAND_DISPLAY XAUTHORITY XDG_CURRENT_DESKTOP XDG_SESSION 2>&1 &
/usr/bin/systemctl --user import-environment DISPLAY WAYLAND_DISPLAY XAUTHORITY XDG_CURRENT_DESKTOP XDG_SESSION2>&1 &

# Polkit agent
/usr/libexec/polkit-gnome-authentication-agent-1 2>&1 &

# Gnome keyring
/usr/bin/gnome-keyring-daemon --start --components=ssh 2>&1 &

# Set desktop wallpaper
/usr/bin/hyprpaper 2>&1 &

# Monitor management
/usr/bin/kanshi >/dev/null 2>&1 &

# Launch a panel such as yambar or waybar
/usr/bin/waybar >/dev/null 2>&1 &
/usr/bin/waybar -c ~/.config/waybar/config2.jsonc 2>&1 &

# Network panel icon
/usr/bin/nm-applet --indicator 2>&1 &

# User autostart applications
flatpak run dev.vencord.Vesktop 2>&1 &
orage -t 2>&1 &

# Clipboard
wl-paste --type text --watch cliphist store 2>&1 &
wl-paste --type image --watch cliphist store 2>&1 &
wl-paste -pw wl-copy 2>&1 &

# Notifications
dunst >/dev/null 2>&1 &

# Lock screen after 5 minutes; turn off display after another 5 minutes.
#
# Note that in the context of idle system power management, it is *NOT* a good
# idea to turn off displays by 'disabling outputs' for example by
# `wlr-randr --output <whatever> --off` because this re-arranges views
# (since a837fef). Instead use a wlr-output-power-management client such as
# https://git.sr.ht/~leon_plickat/wlopm
swayidle -w \
	timeout 1800 'swaylock -f' \
	timeout 1805 'wlr-randr \
    --output HDMI-A-1 --pos 0,0 --off \
    --output DP-3 --pos 2560,0 --off' \
	resume 'wlr-randr \
    --output HDMI-A-1 --pos 0,0 --on \
    --output DP-3 --pos 2560,0 --on' \
	before-sleep 'swaylock -f' 2>&1 &

# Dirty hack so swayidle doesn't start when audio playing
#sway-audio-idle-inhibit 2>&1 &

#/usr/libexec/ibus-ui-gtk3 --enable-wayland-im --exec-daemon --daemon-args "--xim --panel disable" 2>&1 &

# Autoreload waybar if we change the style path
systemctl --user start reload-waybar.path 2>&1 &

# Disk Management (mainly for handling my external hard drive)
udiskie --no-automount --no-notify --tray --appindicator 2>&1 &

# Start desktop portal
/usr/libexec/xdg-desktop-portal-gtk -r &
/usr/libexec/xdg-document-portal -r &
/usr/libexec/flatpak-portal --no-idle-exit -r &
sh -c 'sleep 5; systemctl --user start xdg-desktop-portal' &
sh -c 'sleep 5; systemctl --user start xdg-desktop-portal-wlr' &
