-- Load Lazy.nvim
local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"

if not (vim.uv or vim.loop).fs_stat(lazypath) then
  vim.fn.system({
    "git",
    "clone",
    "--filter=blob:none",
    "https://github.com/folke/lazy.nvim.git",
    "--branch=stable", -- latest stable release
    lazypath,
  })
end

vim.opt.rtp:prepend(lazypath)

vim.g.mapleader = " "       -- Make sure to set `mapleader` before lazy so your mappings are correct
vim.g.maplocalleader = "\\" --Same for `maplocalleader`

-- Plugins
require("lazy").setup(
{{import = "user.plugins"}, {import = "user.plugins.lsp"}},
{
    -- Lazy options --
    ---@type number? limit the maximum amount of concurrent tasks
    git = {
      -- defaults for the `Lazy log` command
      -- log = { "--since=3 days ago" }, -- show commits from the last 3 days
      log = { "-8" }, -- show the last 8 commits
      timeout = 3000, -- kill processes that take more `timeout` number in seconds
      url_format = "https://github.com/%s.git",
      -- lazy.nvim requires git >=2.19.0. If you really want to use lazy with an older version,
      -- then set the below to false. This should work, but is NOT supported and will
      -- increase downloads a lot.
      filter = true,
    },
    install = {
      -- install missing plugins on startup. This doesn't increase startup time.
      missing = true,
      -- try to load one of these colorschemes when starting an installation during startup
      colorscheme = { "catppuccin" },
    },
    ui = {
      -- The border to use for the UI window. Accepts same border values as |nvim_open_win()|.
      border = "rounded",
    },
    checker = {
      -- automatically check for plugin updates
      enabled = true,
      concurrency = 0, ---@type number? set to 1 to check for updates very slowly
      notify = false,  -- get a notification when new updates are found
    },
    change_detection = {
      -- automatically check for config file changes and reload the ui
      enabled = true,
      notify = false, -- get a notification when changes are found
    },
})
