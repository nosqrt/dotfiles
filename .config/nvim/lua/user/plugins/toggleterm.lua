return {
    "akinsho/toggleterm.nvim",
    version = "*",
    config = function()
        local toggleterm = require('toggleterm')
        local mocha = require("catppuccin.palettes").get_palette("mocha")
        toggleterm.setup({
            open_mapping = [[<c-t>]],
            insert_mapping = true,    -- whether or not the open mapping applies in inset mode
            terminal_mapping = true,  -- whether or not the open mapping applies in the opened terminals
            direction = "horizontal", -- "verical" | "horizontal" | "tab" | "float"
            float_opts = {
                border = "curved"     -- "single" | "double" | "shadow" | "curved"
            },
            highlights = {
                FloatBorder = {
                    guifg = mocha.peach,
                    guibg = mocha.base
                }
            }
        })
    end,
    keys = {
        { "<C-t>", "<cmd>ToggleTerm<CR>", desc = "Open/close ToggleTerm" },
    }
}
