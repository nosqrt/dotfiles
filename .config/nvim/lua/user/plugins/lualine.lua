return {
    "nvim-lualine/lualine.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    config = function()
        local lualine = require("lualine")
        local lazy_status = require("lazy.status")

        -- ==================
        -- Functions for statusline
        -- ==================
        local hide_in_width = function()
            return vim.fn.winwidth(0) > 80
        end

        local branch = {
            "branch",
            icons_enabled = true,
            icon = ""
        }

        local diagnostics = {
            "diagnostics",
            sources = { "nvim_diagnostic" },
            sections = { "error", "warn" },
            symbols = { error = " ", warn = " " },
            colored = true,
            update_in_insert = false,
            always_visible = true,
        }

        local diff = {
             "diff",
              colored = false,
              symbols = { added = " ", modified = " ", removed = " " }, -- changes diff symbols
              cond = hide_in_width
        }

        local mode = {
            "mode",
            fmt = function(str)
                return "-- " .. str .. " --"
            end,
        }

        local packages = {
            lazy_status.updates,
            cond = lazy_status.has_updates,
            color = { fg = "#ff9E64" }
        }

        local progress = function()
              local current_line = vim.fn.line(".")
              local total_lines = vim.fn.line("$")
              local chars = { "__", "▁▁", "▂▂", "▃▃", "▄▄", "▅▅", "▆▆", "▇▇", "██" }
              local line_ratio = current_line / total_lines
              local index = math.ceil(line_ratio * #chars)
              return chars[index]
        end

        lualine.setup ({
            options = {
                icons_enabled = true,
                theme = 'catppuccin',
                -- These enclose the "bubbles". Without this, the bar becomes a solid colour.
                section_separators = { left = '', right = '' },
                component_separators = { left = '', right = '' }
            },
            sections = {
                lualine_a = { { 'mode', seperator = { left = '' }, padding_right = 2 } },
                lualine_b = { branch, diagnostics },
                lualine_c = { mode },
                lualine_x = { packages },
                lualine_y = { diff, 'filetype', 'encoding', progress },
                lualine_z = { { 'location', separator = { right = '' }, left_padding = 2 } }
            },
        })
    end,
}
