return {
    "akinsho/bufferline.nvim",
    dependencies = { "nvim-tree/nvim-web-devicons" },
    version = "*",
    config = function()
        local bufferline = require("bufferline")
        local mocha = require("catppuccin.palettes").get_palette("mocha")

        bufferline.setup({
            options = {
                mode = "buffers",
                themable = true,
                indicator = {
                    style = 'underline'
                },
                seperator_style = 'slant',
                always_show_bufferline = true,
                auto_toggle_bufferline = false
            },
            highlights = require("catppuccin.groups.integrations.bufferline").get {
                style = { "italic", "bold" },
                custom = {
                    mocha = {
                        background = { fg = mocha.text }
                    },
                    latte = {
                        background = { fg = "#000000" }
                    }
                },
                buffer_selected = { sp = 'Pink' }
            }
        })
    end
}
