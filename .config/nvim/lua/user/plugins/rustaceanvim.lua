-- keymaps configuration located at after/ftplugin/rust.lua
return {
    "mrcjkb/rustaceanvim",
    version = "^4",
    lazy = false,
    dependencies = {
        "neovim/nvim-lspconfig",
        "mfussenegger/nvim-dap",
        "nvim-treesitter/nvim-treesitter",
        "Saecki/crates.nvim",     -- crates integration
        "akinsho/toggleterm.nvim" -- for use with executor
    },

    config = function()
        -- codelldb integration
        local mason_path = vim.fn.glob(vim.fn.stdpath("data") .. "/mason/")
        local codelldb_path = mason_path .. "bin/codelldb"
        local liblldb_path = mason_path .. "packages/codelldb/extension/lldb/lib/liblldb.so"

        vim.g.rustaceanvim = {
            tools = {
                executor = require("rustaceanvim/executors").toggleterm,
                reload_workspace_from_cargo_toml = true,
                enable_clippy = true,
                runabbles = {
                    use_telescope = true
                },
                inlay_hints = {
                    auto = true,
                    only_current_line = false,
                    show_parameter_hints = true,
                    parameter_hints_prefix = "<- ",
                    show_hints_prefix = "=> ",
                    max_len_align = false,
                    max_len_align_padding = 1,
                    right_align = 1,
                    right_align_padding = 7,
                    highlight = "Comment"
                },
                hover_actions = {
                    border = "rounded"
                },
                on_initialized = function()
                    vim.api.nvim_create_autocmd({ "BufWritePost", "BufEnter", "CursorHold", "InsertLeave" }, {
                        pattern = { "*.rs" },
                        callback = function()
                            local _, _ = pcall(vim.lsp.codelens.refresh)
                        end
                    })
                end
            },
            -- Debugging
            dap = {
                adapter = require("rustaceanvim.config").get_codelldb_adapter(codelldb_path, liblldb_path)
            },
        }
    end
}
