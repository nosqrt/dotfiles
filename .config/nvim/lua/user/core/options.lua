-- :help options
vim.opt.backup = false                              -- creates a backup file
vim.opt.clipboard = "unnamedplus"                   -- allows neovim to access the system clipboard
vim.opt.cmdheight = 2                               -- more space in the neovim command line for displaying messages
-- vim.opt.colorcolumn = 105
vim.opt.completeopt = { "menuone", "noselect" }     -- mostly just for cmp
vim.opt.conceallevel = 0                            -- so that `` is visible in markdown files
vim.opt.fileencoding = "utf-8"                      -- the encoding written to a file
vim.opt.hlsearch = true                             -- highlight all matches on previous search pattern
vim.opt.ignorecase = true                           -- ignore case in search patterns
vim.opt.mouse = "a"                                 -- allow the mouse to be used in neovim
vim.opt.pumheight = 10                              -- pop up menu height
vim.opt.showmode = true                             -- the -- INSERT -- etc. in status line
vim.opt.showtabline = 2                             -- always show tabs
vim.opt.smartcase = true                            -- matches upper and lower case letter when using "/" "?" "n" "n" etc.
vim.opt.smartindent = true                          -- do smart autoindenting when starting a new line
vim.opt.splitbelow = true                           -- force all horizontal splits to go below current window
vim.opt.splitright = true                           -- force all vertical splits to go to the right of current window
vim.opt.swapfile = false                            -- creates a swapfile for current buffer
-- vim.opt.termguicolors = true                     -- set term gui colors (most terminals support this)
vim.opt.timeoutlen = 1000                           -- time to wait for a mapped sequence to complete (in milliseconds)
vim.opt.undofile = true                             -- enable persistent undo
vim.opt.updatetime = 300                            -- faster complete (4000ms default)
vim.opt.writebackup = false                         -- if a file is being edited by another program (or was written to file white editing with another program),
vim.opt.expandtab = true                            -- convert tabs to spaces
vim.opt.shiftwidth = 4                              -- the number of spaces inserted for each indentation
vim.opt.tabstop = 4                                 -- number of spaces inserted for a tab
vim.opt.cursorline = true                           -- highlight the current line
vim.opt.number = true                               -- set numbered lines
vim.opt.relativenumber = false                      -- set relative numbered lines
vim.opt.signcolumn = "yes"                          -- always show the sign column, other it would shift the text time
vim.opt.wrap = true                                 -- display lines as one long line
vim.opt.scrolloff = 8                               -- number of screen lines to keep above and below the cursor
vim.opt.sidescrolloff = 8
vim.opt.guifont = "Inconsolata Nerd Font:h11"       -- font used for graphical neovim applications

vim.opt.rtp:append("/usr/share/vim/vimfiles")

--vim.api.nvim_create_autocmd("BufReadPost", {
--  group = vim.g.user.event,
--  callback = function(args)
--    local valid_line = vim.fn.line([['"']]) >= 1 and vim.fn.line([['"']]) < vim.fn.line("$")
--    local not_commit = vim.b[args.buf].filetype ~= "commit"
--
--    if valid_line and not_commit then
--      vim.cmd([[normal! g'"']])
--    end
--  end,
--})
