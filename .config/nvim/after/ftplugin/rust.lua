local bufnr = vim.api.nvim_get_current_buf()
vim.keymap.set(
    "n",
    "<leader>a",
    function()
        vim.cmd.RustLsp("codeAction")
    end,
    {
        desc = "Rust CodeAction",
        silent = true,
        buffer = bufnr,
    }
)
