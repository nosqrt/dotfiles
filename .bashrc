### Source (ble.sh)
source "$HOME/.local/share/blesh/ble.sh"
source "$HOME/.bash_profile"

### Environment
export NVIM_GTK_PREFER_DARK_THEME=1
export PERL5LIB=/home/dani/perl5/lib/perl5

# Colour the less command
export LESS='--mouse --wheel-lines=4 -R'
export LESS_TERMCAP_mb=$'\E[01;31m'
export LESS_TERMCAP_md=$'\E[01;31m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;44;37m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;33m'

### Aliases
# Gentoo management
alias ec='sudo emerge -c && eclean-dist -d'
alias egrep='egrep --colour=auto'
alias eixs='sudo eix-sync -a'
# Both of the below use the '--getbinpkg' switch (also seen as the -g option)
alias eu='sudo emerge -uqDN --complete-graph --with-bdeps=y --keep-going --deep @world'
alias euv='sudo emerge -uvDN --complete-graph --with-bdeps=y --keep-going @world'
alias esync='sudo -E eix-sync'
alias erc='sudo emerge --rage-clean'
alias eua='esync; euv'

# Useful color to common command line tools
alias fgrep='fgrep --colour=auto'
alias grep='grep --colour=auto'
alias ls='ls --color'

# Common ls shortcuts
alias l='ls -l'
alias ll='ls -lFha'

# Program Aliases
alias vim='lvim'
alias kdenlive='flatpak run org.kde.kdenlive'
#alias mpv='mpv --player-operation-mode=pseudo-gui'

# Dotfiles management
alias config='/usr/bin/git --git-dir=/home/dani/.cfg/ --work-tree=/home/dani'
