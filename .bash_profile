source /etc/profile

GPG_TTY=""$(tty)

export GPG_TTY
export SSH_AUTH_SOCK=$XDG_RUNTIME_DIR/keyring/ssh
export SSH_ASKPASS=/usr/libexec/seahorse/ssh-askpass
