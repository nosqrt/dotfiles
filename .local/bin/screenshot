#!/usr/bin/env bash

# This is simplified version of the screenshot command used in my rofi
# script. The intention of this script is for use with labwc's rc.xml.

time="$(date +%Y%m%d_%I%M%S)"

# Need to keep  the area from slurp as-is because grim requires coordinate data.
area="$(slurp)"
# Geometry is just grabbing the last part of $area e.g. 1194x692. This is used
# in our filename.
geometry="$(sed -rn 's/.*[[:blank:]]([[:digit:]]+x[[:digit:]]+).*/\1/p' <<< "${area}")"
dir="$(xdg-user-dir PICTURES)/Screenshots"
file="Screenshot_${time}_${geometry}.png"
scrot="grim -g \"${area}\" ${dir}/${file}"

function notify_view() {
    notify_cmd_shot='dunstify -u low'
    # Coproc will cause a warning if we don't wait for the async command to
    # finish. Simplest way wait is via `wait $!`
    # https://unix.stackexchange.com/questions/337151/bash-unsets-pid-variable-before-i-can-wait-on-coproc
    coproc { ${notify_cmd_shot} "${dir}/${file}" "Copied to clipboard."; }
    wait $!
    coproc { ${notify_cmd_shot} -i "${dir}/${file}" "Screenshot Saved."; }
    wait $!
}

function copy_shot() {
    wl-copy -t image/png < "${file}"
}

function shotnow() {
	  coproc {
        cd "${dir}" && eval "${scrot}"
        copy_shot
	      notify_view;
    }
    wait $!
}

shotnow
