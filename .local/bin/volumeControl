#!/usr/bin/env bash
# shellcheck disable=SC2317
# ^ Disables "function not reachable" because of eval

function get_volume() {
    WP_OUTPUT=$(wpctl get-volume @DEFAULT_AUDIO_SINK@)

    # 3 capture groups
    #   [1] = number before the `.` seperator
    #   [2] = number after the `.` seperator
    #   [3] = if muted, `"MUTED"` is set (check with [[ -n ${BASH_REMATCH[3]} ]] )
    if [[ $WP_OUTPUT =~ ^Volume:[[:blank:]]([0-9]+)\.([0-9]{2})([[:blank:]].MUTED.)?$ ]]; then
        if [[ -n ${BASH_REMATCH[3]} ]]; then
            # Return the word, "MUTE" if there's a 3rd capture group - only occurs when muted
            echo "MUTE"
        else
            # Return the volume as base 10
            echo "$((10#${BASH_REMATCH[1]}${BASH_REMATCH[2]}))"
        fi
    fi
}

function volume() {
    CURRENT_VOLUME="$(get_volume)"
    
    # Add a '%' to the end if not muted. Assign to VOLUME.
    if [ "${CURRENT_VOLUME}" == "MUTE" ] || [ "${CURRENT_VOLUME}" == 0 ]; then
        printf "%s" "MUTE"
    else
        printf "%s" "${CURRENT_VOLUME}%"
    fi
}

function current_sink() {
    wpctl status \
        | sed -rn '/Sinks:/{:a;N;/.*Sources:/!ba; s/[├─│└]+//g; /C/p}' \
        | sed -rn 's/^[[:blank:]]+(.*) \[vol:.*/\1/p' \
        | sed -rn 's/^\*[[:digit:][:blank:]\.]+(.*)/\1/p' 
}

function get_icon() {
    volume=$(get_volume)
    # nameref indirection (similar to \$ref in perl): `help declare`
    local -n icons=$1

    # `icon[0]` is for use in tint2, `icon[1]` is for use in waybar, `icon[2]` is current volume
    case 1 in
        $((volume == "MUTE" || volume == 0)))
            icons=("audio-volume-muted-symbolic" "󰖁 ")
        ;;
        $((volume > 0 && volume <= 35)))
            icons=("audio-volume-low-symbolic" "󰖀 ")
        ;;
        $((volume > 35 && volume <= 70)))
            icons=("audio-volume-medium-symbolic" "󰕾 ")
        ;;
        $((volume>70)))
            icons=("audio-volume-high-symbolic" " ")
        ;;
    esac

    icons+=("$volume")
}

function send_notification() {
    get_icon icon

    case $1 in
        volume)
            [ "${icon[2]}" == 0 ] \
                && eval muted_notification \
                || eval unmuted_notification "${icon[0]}" "${icon[2]}%" "Volume:"
        ;;
        mute)
            [ "${icon[2]}" == "MUTE" ] \
                && eval muted_notification \
                || eval unmuted_notification "${icon[0]}" "${icon[2]}%" "Un-Muted:"
        ;;
        icon)
            printf '%s' "${icon[0]}"
        ;;
        symbol)
            printf '%s' "${icon[1]}"
        ;;
    esac
}

function unmuted_notification() {
    dunstify -t 1400 -i "$1" -u normal -h string:x-dunst-stack-tag:Audio -h int:value:"$2" "$3 $2"
    canberra-gtk-play -i audio-volume-change -d "changeVolume"
}

function muted_notification() {
    dunstify -t 1400 -i audio-volume-muted-symbolic -u normal -h string:x-dunst-stack-tag:Audio Mute
    canberra-gtk-play -i audio-volume-change -d "changeVolume"
}

function x11_display() {
    # For use in tint2.
    #   STDOUT: Ouputs an icon and current volume.
    #   STDERR: The currently selected sink. This will output to the tooltip.
    printf "/usr/share/icons/Faenza/status/scalable/%s.svg\n%s\n" \
        "$(send_notification 'icon')" \
        "$(volume)"
    printf "%s\n" "$(current_sink)" >&2
}

function wayland_display() {
    # Waybar can use json formatting.
    # Make sure you have, "return-type": "json" in your custom-script config.
    printf '{"text":"%s","tooltip":"%s","class":"%s"}\n' \
        "$(send_notification 'symbol')$(volume)" \
        "$(current_sink)" \
        "$1"
}

case $1 in
    up)
        wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%+ -l 1.0
        send_notification "volume"
    ;;
    down)
        wpctl set-volume @DEFAULT_AUDIO_SINK@ 5%- -l 1.0
        send_notification "volume"
    ;;
    mute)
        wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle
        send_notification "mute"
        printf "%s\n" "$(volume)"
    ;;
    volume)
        if [ "${XDG_SESSION_TYPE}" == "x11" ]; then
            printf "%s\n" "$(x11_display)"
        else
            eval pgrep -f "swapAudioSink" > /dev/null && status='switch' || status='output'
            printf "%s\n" "$(wayland_display "${status}")"
            pkill -RTMIN+11 waybar
        fi
    ;;
    switch)
        if eval pgrep -f "swapAudioSink" >/dev/null; then
            pkill rofi
        else
            eval ~/.local/bin/swapAudioSink
        fi
    ;;
esac

exit 0
