<img src="https://gitlab.com/nosqrt/dotfiles/-/raw/main/.images/Screenshot_20240502_064504_2559x1080.png" align="center" width="1024" height="432"/>

# Labwc and Openbox dotfiles
I've switched over to Labwc near permanently but still sometimes use openbox. Openbox
has been an amazing WM over the years but I wanted to see how things are in Wayland.
So far, it's been a fun time! :3

## System
### OS and Theme

<details>
<summary>Click to expand</summary>

| Name                 | Package                                                                                                                                |
|---                   | ---                                                                                                                                    |
| **Operating System** | Gentoo                                                                                                                                 |
| **Gentoo Profile**   | 23.0/desktop/gnome/systemd (stable)                                                                                                    |
| **Font**             | [Inconsolata Go Nerd Font](https://github.com/ryanoasis/nerd-fonts)                                                                    |
| **Icon Theme**       | x11-themes/qogir-icon-theme::guru<br>x11-themes/faenza-icon-theme<br>x11-themes/numix-icon-theme<br>x11-themes/numix-icon-theme-circle |
| **GTK Theme**        | [Catppuccin-Mocha-Standard-Yellow-dark](https://github.com/catppuccin/gtk)                                                             |
| **Openbox Theme**    | [Catppuccin-Mocha](https://github.com/catppuccin/openbox)<br>(slightly customised)                                                     |

</details>

### X11/Wayland Non-Specific Packages

<details>
<summary>Click to expand</summary>

| Name                 | Package                                                                                |
| ---                  | ---                                                                                    |
| **Audio**            | media-video/wireplumber<br>media-video/pipewire                                        |
| **Notifications**    | x11-misc/dunst                                                                         |
| **Terminal**         | x11-terms/kitty<br>x11-terms/alacritty                                                 |
| **Multiplexer**      | app-misc/tmux                                                                          |
| **File Manager**     | xfce-base/thunar                                                                       |
| **Browser**          | www-client-firefox-bin                                                                 |
| **E-mail**           | mail-client/thunderbird-bin                                                            |
| **Game Launcher**    | [Lutris Flatpak](https://flathub.org/apps/net.lutris.Lutris)<br>[Steam Flatpak](https://flathub.org/apps/com.valvesoftware.Steam) |
| **Network Applet**   | gnome-extra/nm-applet<br>*nm-applet --appindicator*                                    |
| **Editor**           | app-editors/neovim<br>app-editors/neovide::brushy                                      |
| **Calender**         | app-office/orage<br>*orage -t*                                                         |
| **Discord**          | net-im/discord                                                                         |
| **IRC**              | net-irc/weechat                                                                        |
| **Keyring**          | gnome-base/gnome-keyring                                                               |
| **Mount Management** | sys-fs/udiskie                                                                         |
| **Shell**            | app-shells/bash<br>[ble.sh](https://github.com/akinomyoga/ble.sh)                      |

#### Notes: Flatpak Game Launchers

I've opted to use the steam and lutris flatpaks as I can't be bothered dealing
with a multilib system. I formerly used `games-util/steam-meta::steam-overlay`
but that required installing [a heinous amount of abi_x86_32](https://wiki.gentoo.org/wiki/Steam#USE_flags)
(otherwise known as multilib packages). Keeping Steam in a container was the much
more easy-to-manage solution. Flatpak simply was the easiest way to install and
configure a containerised Steam.

If you decide to go down this route, read the information on the
[gentoo wiki](https://wiki.gentoo.org/wiki/Steam#Flatpak)
and ensure that you have portals (e.g. gui-libs/xdg-desktop-portal-gtk) installed
for the best experience.

I also formerly used `games-util/lutris` for similar reasons; namely that
`wine-vanilla` pulls in a lot of abi_x86_32 rebuilds. I'd suggest taking a
peek into my [.profile](.profile) as setting `$PATH` and `$XDG_DATA_DIRS`
correctly will allow you to launch both steam and lutris via rofi.

</details>

# X11 - Openbox
Since I'm not actively tinkering with my openbox setup, this list will likely remain
the same indefinitely.

My openbox setup heavily relies upon [systemd-user-units](https://wiki.archlinux.org/title/systemd/User)
to function and requires a little more configuration than simply copy+pasting my dotfiles to use.

## Software

<details>
<summary>Click to expand</summary>

| Name                    | Package                                                                         |
| ---                     | ---                                                                             |
| **Window Manager**      | x11-wm/openbox                                                                  |
| **Appearance Manager**  | x11-misc/obconf<br>lxde-base/lxappearance-obconf                                |
| **Openbox Client Menu** | x11-misc/obmenu-generator<br>Using piped menus via *obmenu-generator -i*        |
| **Bar**                 | x11-misc/tint2                                                                  |
| **Wallpaper**           | x11-misc/nitrogen                                                               |
| **Launcher**            | gui-apps/rofi-wayland::guru<br>Run with *rofi -x11* and the build needs editing |
| **Clipboard**           | x11-misc/parcellite                                                             |
| **LockScreen**          | x11-misc/xscreensaver                                                           |
| **Screenshot**          | gui-apps/grim                                                                   |
| **Monitor Management**  | lxde-base/lxrandr                                                               |
| **Portals**             | sys-apps/xdg-desktop-portal-gtk                                                 |

</details>

# Wayland - Labwc
My waybar config notably uses fontawesome as little as possible. I've opted instead
to use nerd-font symbols (where possible) as they are:
1. Free - Font Awesome is a hassle as many good symbols are paid-for-use.
2. Less hassle to find - Nerd Fonts' website is quite good and don't need to worry
   about price.
3. Generally cause me less issues when rendering.

## Software
As Wayland is still *new*, the list of programs below are subject to change. These
programs are simply what I'm currently using. The only thing I don't see changing
is the compositor. This is simply a list of software that I can remember off the top
of my head. It should be complete enough to give you an idea of what's being used.

<details>
<summary>Click to expand</summary>

| Name                   | Package                                                               |
| ---                    | ---                                                                   |
| **Compositor**         | gui-wm/labwc::wayland-desktop                                         |
| **Labwc Client Menu**  | [labwc-menu-generator](https://github.com/labwc/labwc-menu-generator) |
| **Appearance Manager** | [labwc-tweaks](https://github.com/labwc/labwc-tweaks)                 |
| **Bar**                | gui-apps/waybar                                                       |
| **Wallpaper**          | gui-apps/hyprpaper::wayland-desktop                                   |
| **Launcher**           | gui-apps/rofi-wayland::guru                                           |
| **Clipboard**          | gui-apps/wl-clipboard                                                 |
| **LockScreen**         | gui-apps/swayidle<br>gui-apps/swaylock<br>gui-apps/wlr-randr::guru    |
| **LockScreen Idle**    | gui-apps/sway-audio-idle-inhibit::guru                                |
| **Screenshot**         | gui-apps/grim<br>gui-apps/slurp                                       |
| **Monitor Management** | gui-apps/kanshi                                                       |
| **Portals**            | gui-libs/xdg-desktop-portal-wlr<br>sys-apps/xdg-desktop-portal-gtk    |

## Other Notable Software
| Name                       | Package                  |
| ---                        | ---                      |
| **Gui Monitor Management** | gui-apps/wdisplays::guru |
| **Wayland Information**    | app-misc/wayland-utils   |

</details>

# Custom Scripts
All of the scripts I use are found within `.local/bin`. Most of these are written by
yours truly.

## discordMonitor
This script is intended to be used with my openbox configuration. Occasionally Discord
would randomly crash and I wanted something that would restart it when that occurred.
Worse yet, sometimes it didn't crash and tint2 removed it from the system tray; making
it impossible to bring discord out of the tray. (It doesn't create a window tab when
it's minimised).

The script follows `journalctl -u tint2 --user --follow -n0 -o cat` as I worked out
that it would be be simplest to watch the tint2 logs for the icon being removed from
the system tray.

An intersting side effect of having this script run in the background is that if you
try to exit Discord while it's running, it'll simply re-open Discord.

To execute this script, it's launched via it's own systemd-user-unit file.
```
systemctl --user start discord.service
```
or to start immediately and start on user login:
```
systemctl --user enable --now discord.service
```

## screenshot
This script is designed for screenshotting an area, creating a dunst notification and
saving it to `$(xdg-user-dir PICTURES)/Screenshots` folder. This version of the script
is for _**WAYLAND ONLY**_ as both grim and slurp do not work in X11.<br>

To use this script, create a keybind in your `~/.config/labwc/rc.xml` file and it
should work.

## swapAudioSink
My personal setup has two audio output sources: Speakers and Headphones. I wanted a
quick way to change between them without needing to type in `wpctl status` and 
`wpctl set-default <number>` every time.

My solution was to use the power of sed to remove all of the junk info from
`wpctl status`, index each line from the sed output using a bash regex, then adding
the info as a key along with it's corresponding sink number as the pair to a bash
hash. It then prints the `key`s of the hash into rofi and blam. You now have a
visual way to change your audio sink.

This script will work under both X11 and Wayland. I've got it setup as a right click
on my audio tint2 executor/waybar custom module.

NOTE: You will need to use the `~/.local/bin/volumeControl switch` command to use this
properly in Wayland/Labwc as right clicking the custom module doesn't exit rofi.
X11/Openbox doesn't have this issue and you're free to call the script as-is.

## volumeControl
My system is using pipewire + wireplumber. When I began writing this script, there
weren't any real good bar modules out there that satisfied my needs. Over the years,
the volumeControl script has looked drastically different; for instance, bash didn't
have native regex and `BASH_REMATCH` support which required me to instead do some loop
wizardry with sed to build an associated array. It also used to use `pactl` before
pipewire + wireplumber even existed. History aside, lets discuss what this script does.

The volumeControl script does exactly as it's name suggests: gives you 5 different
actions you can take when it comes to controlling audio. These actions are:
- 1. volumeControl up
- 2. volumeControl down
- 3. volumeControl mute
- 4. volumeControl volume
- 5. volumeControl switch

The `volumeControl {up,down,mute}` all do some things the same. They all create a
dunst notification with `dunstify`, which shows an icon, the current volume/muted
status along with a progress bar, changes the volume/mute status then exits.

`volumeControl up` and `volumeControl down` both do the same fundamental thing: raise
or lower the audio level. The script is set to raise/lower the volume by 5%.

`volumeControl mute` calls `wpctl set-mute @DEFAULT_AUDIO_SINK@ toggle` which as the
command implies, toggles the default sinks mute status.

The `volumeControl volume` command acts differently depending on if you're using
X11/Openbox or Wayland/Labwc. If you call the volume function under X11, the output
will include the following in this order:
 - Path to a icon corresponding to the current volume (current using Faenza)
 - The current volume
 - The current sink which is redirected to STDERR, or `>&2` if you'd prefer

The reason why it outputs the information this way is because that's what a tint2
expectes from an executor. The STDERR will be displayed as a tooltip by tint2.

In Wayland, it'll output a JSON which includes:
- "text": "{icon}volume%"
- "tooltip": "{current_sink}"
- "class": "{sink_current_state}"

Like with the tint2 example above, this is what waybar custom module expects from the
scripts output. The "text" key contains a nerdfont icon corresponding to the current
volume. The "tooltip" key holds the currently set default audio sink. The "class" key
changes depending on if `volumeControl switch` has been called. This can be "switch"
or "output". This allows for waybar css styling based on the state of the rofi switcher
script being run. If the rofi switcher script is running, the background colour of the
volumeControl waybar custom module is set to a yellow. When it's inactive, it's set to
catppuccin mochas' "mantle" colour.

Last but not least, the `volumeControl switch` option simply checks if the `swapAudioSink`
is running. If it's not running, it'll launch `swapAudioSink`. If it's running, it'll
`pkill rofi`.

To fully explain what's going on in the `volumeControl volume` command above, that command
is re-triggered upon any of the other commands being triggered. This allows for us to
effectively determine the state of the `swapAudioSink` script being run.

Long story short: I managed to completely overcomplicate audio controls which should be
simple 🥹

So you might be asking, "hey, but how do I *USE* any of this???" To that I say: both
keybinds in your `~/.config/labwc/rc.xml` or `~/.config/openbox/rc.xml`. Waybar and
tint2 specifically have their own configurations which can be binded to scroll wheel or
mouse click events.

## weather
The current weather script just grabs output from https://wttr.in. I have yoinked a
different weather script which utilises openWeather's API but I don't know if I want to
upload a file with my openWeather API key to the internet so freely. I may refactor it
and store that API key in a different file at a later date.
